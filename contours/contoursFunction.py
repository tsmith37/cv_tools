import numpy as np
import cv2

image = cv2.imread('image.jpg')

gray_scale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray_scale, 127, 255, 0)
newImage, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, c2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(image, contours, -1, (0,255,0), 3)
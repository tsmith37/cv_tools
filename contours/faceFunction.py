import numpy as np
import cv2

image = cv2.imread('image.jpg')
face_cascade = cv2.CascaseClassifier('haarcascase_frontalface_default.xml')

gray_scale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#Last two parameters confuse me - Don't think required (1 and 2)?
faces_list = face_cascade.detectMultiScale(gray_scale)

print('Number of faces:', len(faces_list))
for (x,y,w,h) in faces_list:
    print('Location:', (x+w/2), (y+h/2))